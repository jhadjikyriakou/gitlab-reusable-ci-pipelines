# Automatic setup for Continuous Integration  

Combines Gitlab for Continuous Integration,Continuous Deployment and Continuous Delivery with built-in Code Quality feature, and [Ansible](https://www.ansible.com/) for the server setup.Supports nodejs and django apps.

## Server and Gitlab setup

To setup the server, Ansible is required on the host that will be used to access the server/remote host.
It can be installed by issuing `pip install ansible`.  

1. In the `gitlab-reusable-ci-pipelines/Ansible/hosts` (inventory) file, define the staging host, inside the `staging` host group. 
2. `cd` into the `gitlab-reusable-ci-pipelines/Webapp/` directory, open `index.html` file and fill in the form and then download `deploy-app-vars.yml` file  with the desired variable values and after that move `deploy-app-vars.yml` file from Downloads to `gitlab-reusable-ci-pipelines/Ansible/` directory.
3. `cd` into the `gitlab-reusable-ci-pipelines/Ansible/` directory and issue the command: `ansible-playbook deploy-ci-staging.yml -i hosts --extra-vars "ci_user=<ci_user_name>"`,
where ci_user_name is the user that will generate SSH key on server/remote host. SSH and access is required. The user must be able to run sudo without a password.  
4. Copy the SSH private key using `cat .ssh/id_rsa` on the server/remote host, add a gitlab variable named **SSH_PRIVATE_KEY** on the repository that will be used for the CI and copy-paste the key.  
5. Add a variable named **DEPLOYMENT_SERVER** in gitlab with options **heroku** for deployment on heroku or **server** for deployment on a server/remote host.  
6. Add a variable named **HEROKU_STAGING_API_KEY** if the deployment in going to be on heroku with the value you get when running `heroku auth:token` command on your terminal which is connected to your heroku account.Also add a variable **HEROKU_PROD_API_KEY** for production using the api key stored in heroku account settings.
7. Add a variable named **HEROKU_APP_STAGING** if the deployment in going to be on heroku containing the name of the heroku app where is going to be the deployment.
8. Add a variable **SSH_USER** with value the name of the user who will connect to the server/remote host and a variable **SSH_HOST** that will contain the IP or Hostname of the server/remote host. 
9. Add variables **DB_TYPE** with options **mysql**  and **postgres**,variables **MYSQL_PASS** , **MYSQL_DB** for mysql password and db name, and variables **PG_DB** , **PG_PASS** , **PG_USER** for postgres name, password and user respectively.
10. Αdd variable **APP_NAME** containing the name of the app in order to move into the app's directory after ssh access to the remote host. 
11. Include the desired pipeline file for nodejs or django app on `.gitlab-ci.yml` file and the pipeline will start.  
12. Repeat the same procedure for deployment on the production host adding the variables **SSH_PRIVATE_KEY_PROD** , **HEROKU_APP_PROD** , **SSH_USER_PROD** , **SSH_HOST_PROD**.

**Notes:**  
1. For heroku deployment Ansible setup is not necessary.

