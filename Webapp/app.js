function Save_data_to_file() {
    var inputfields = $('#createform').find('input');
    var emptyfields = false;
    for (var i = 0; i < inputfields.length; i++) {
        if (inputfields[i].value.length === 0) {
            $('label[for=' + inputfields[i].id + ']').addClass('error');
            emptyfields = true;
        }
        else {
            $('label[for=' + inputfields[i].id + ']').removeClass('error');
        }
    }
    if (emptyfields) {
        $(".hidden").removeClass('hidden');
        return;
    }
    $(".hidden").addClass('hidden');
  
    $.ajax({
        type: 'POST',
        url: '/deploy-app-vars',
        data: $('#createform').serialize(),
        success: function (data, status, request) {
            status_url = request.getResponseHeader('status_url');
            $('#Save-data-to-file-button').attr("disabled", true);
           
        }
       
    });
//https://www.encodedna.com/javascript/how-to-save-form-data-in-a-text-file-using-javascript.htm

 // Get the data from each element on the form.
 const app_name = document.getElementById('app_name');
 const app_type = document.getElementById('app_type');
 const repo_url = document.getElementById('repo_url');
 const db_type = document.getElementById('db_type');
 const db_password = document.getElementById('db_password');
 const db_user = document.getElementById('db_user');
 const db_name = document.getElementById('db_name');
 
 // This variable stores all the data.
 let data = 
     '\r app_name: ' + app_name.value + ' \r\n ' + 
     'app_type: ' +app_type.value + ' \r\n ' + 
     'repo_url: ' +repo_url.value + ' \r\n ' + 
     'db_type: ' + db_type.value + ' \r\n ' + 
     'db_password: ' + db_password.value + ' \r\n ' + 
     'db_user: ' + db_user.value + ' \r\n ' + 
     'db_name: ' + db_name.value;
 
 // Convert the text to BLOB.
 const textToBLOB = new Blob([data], { type: 'text/plain' });
 const sFileName = 'deploy-app-vars.yml';	   // The file to save the data.

 let newLink = document.createElement("a");
 newLink.download = sFileName;

 if (window.webkitURL != null) {
     newLink.href = window.webkitURL.createObjectURL(textToBLOB);
 }
 else {
     newLink.href = window.URL.createObjectURL(textToBLOB);
     newLink.style.display = "none";
     document.body.appendChild(newLink);
 }

 newLink.click(); 
}

$(function () {
    $('#Save-data-to-file-button').click(Save_data_to_file);
});
$(function () {
    $('.selectbox').click(function () {
        $(this).focus();
        $(this).select();
    });
});


    